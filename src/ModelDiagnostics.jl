module ModelDiagnostics

"""
    homogeneous_clusters(x; threshold = 1.1)

Find clusters of similar values in the 1D vector x.

A value is added to a cluster if is is within a multiplicative `threshold`
distance from another element in the cluster.

Return an array of arrays where the bottommost array holds the indices for a 
cluster.
"""
function homogeneous_clusters(params; threshold = 1.1)
    idxs = sortperm(params)
    p_sorted = params[idxs]
    clusters = Set{Int}[]
    similar_to_previous = false
    for i = 1:(length(p_sort) - 1)
        if p_sorted[i] * threshold > p_sorted[i + 1]
            if similar_to_previous
                union!(clusters[end], Set(idxs[[i, i + 1]]))
            else
                push!(clusters, Set(idxs[[i, i + 1]]))
            end
            similar_to_previous = true
        else
            similar_to_previous = false
        end
    end
    return sort.(collect.(clusters))
end

export homogeneous_clusters

end # module
